const chai = require('chai');
const chaiHttp = require('chai-http');

const server = require('../server');

chai.use(chaiHttp);

const { expect } = chai;

describe('GraphQL test', () => {

    it('create a new pdv for testing', async () => {
        let isTestFinished1 = false;

        const pdvname = 'PDV For Testing';

        const data = {
            query: `
              mutation {
				  createPDVs(input: [{tradingName: "${pdvname}", ownerName: "test", document: "t1est", coverageArea: [[[[-44.04912,-19.87743],[-44.0493,-19.89438],[-44.04758,-19.90212],[-44.04346,-19.90922],[-44.03385,-19.91923],[-44.01891,-19.92165],[-44.01647,-19.92306],[-44.01436,-19.92319],[-44.01175,-19.92427],[-44.00724,-19.92585],[-43.99909,-19.9185],[-43.99432,-19.91403],[-43.99557,-19.90842],[-43.99582,-19.90285],[-43.99436,-19.89002],[-43.99316,-19.8792],[-43.99436,-19.87371],[-43.99951,-19.86532],[-44.01917,-19.85135],[-44.02801,-19.8545],[-44.03745,-19.85668],[-44.04397,-19.8608],[-44.04912,-19.87743]]]], address: [-43.297337,-23.013538]}]) {
				    	id
				      tradingName
				      ownerName
				      document
				      coverageArea{ 
				        type
				        coordinates
				      }
				      address{ 
				        type,
				     	 coordinates
				      }
				  }
				}
            `
        };

        await chai.request(server)
            .post('/')
            .send(data)
            .then((res) => {
                expect(res).to.have.status(200);
                expect(res.body.data.createPDVs.name).to.be.equal(pdvname);
                isTestFinished1 = true;
            });

        expect(isTestFinished1).is.true;
    });

    it('should test pdv', async () => {
        let isTestFinished = false;

        const id = 1;

        const data = {
            query: `
              {
                pdv(id: ${id}){
			      	id
			      	tradingName
			      ownerName
			      document
			      coverageArea{ 
			        type
			        coordinates
			      }
			      address{ 
			        type,
			     	 coordinates
			      }
			    }
              }
            `
        };

        await chai.request(server)
            .get('/')
            .query(data)
            .send()
            .then((res) => {
                expect(res).to.have.status(200);
                expect(res.body.data.pdvs.id).to.be.equal(id);
                isTestFinished = true;
            });

        expect(isTestFinished).is.true;
    });

    it('should test pdv with wrong id', async () => {
        let isTestFinished = false;

        const id = 100;

        const data = {
            query: `
              {
                pdv(id: ${id}){
			      	id
			      	tradingName
			      ownerName
			      document
			      coverageArea{ 
			        type
			        coordinates
			      }
			      address{ 
			        type,
			     	 coordinates
			      }
			    }
              }
            `
        };

        await chai.request(server)
            .get('/')
            .query(data)
            .send()
            .then((res) => {
                expect(res).to.have.status(200);
                expect(res.body.data.pdvs).is.null;
                isTestFinished = true;
            });

        expect(isTestFinished).is.true;
    });


   
});