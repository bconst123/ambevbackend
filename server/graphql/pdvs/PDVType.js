import {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLList,
  GraphQLID,
  GraphQLString,
  GraphQLInputObjectType,
  GraphQLNonNull,
} from 'graphql'
import {
  PointObject,
  MultiPolygonObject,
} from 'graphql-geojson'

const PDVType = new GraphQLObjectType({
    name: 'PDV',
    description: 'PDV type definition',
    fields: () => ({
      id: { 
        type: new GraphQLNonNull(GraphQLID),
      },
      tradingName: { 
        type: GraphQLString,
      },
      ownerName: { 
        type: GraphQLString,
      },
      document: { 
        type: GraphQLString,
      },
      coverageArea: {
        type: MultiPolygonObject,
      },
      address: {
        type: PointObject,
      }
    }),
});



export {
  PDVType as default,
};
