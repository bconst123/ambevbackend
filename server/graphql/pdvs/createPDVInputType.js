import {
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLList,
  GraphQLFloat,
  GraphQLID,
  GraphQLString,
  GraphQLInputObjectType,
  GraphQLNonNull,
} from 'graphql';

const createPDVInputType = new GraphQLInputObjectType({
  name: 'createPDVInput',
  fields: () => ({
    tradingName: { 
        type: new GraphQLNonNull(GraphQLString),
      },
      ownerName: { 
        type: GraphQLString,
      },
      document: { 
        type: GraphQLString,
      },
      coverageArea: {
        type: new GraphQLList(new GraphQLList(new GraphQLList(new GraphQLList(GraphQLFloat)))), //MultiPolygonObject
      },
      address: {
        type: new GraphQLList(GraphQLFloat), //PointObject
      },
  }),
});

export {
  createPDVInputType as default,
};