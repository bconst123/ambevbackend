import {
  GraphQLList,
  GraphQLID,
  GraphQLNonNull,
  GraphQLFloat,
  GraphQLInt
} from 'graphql';
import { getPDVs, getOnePDV, insidePoligon } from '../../db/PDVsDb';
import PDV from './PDVType';

const PDVQueries = {
  pdv: {
    type: PDV,
    args: {
      id: {
        type: new GraphQLNonNull(GraphQLID),
      },
    },
    resolve: async (source, { id }) => {
      const result = await getOnePDV({ id });
      return result;
    },
  },
  searchpdv: {
    type: PDV,
    args: {
      x: {
        type: new GraphQLNonNull(GraphQLFloat),
      },
      y: {
        type: new GraphQLNonNull(GraphQLFloat),
      },
    },
    resolve: async (source, { x, y }) => {
      const result = await insidePoligon({ x, y });
      return result;
    },
  },
  pdvs: {
    type: new GraphQLNonNull(new GraphQLList(new GraphQLNonNull(PDV))),
    args: {
      role: {
        type: GraphQLInt,
      },
    },
    resolve: async (source, { role }) => {
      const result = await getPDVs();
      if (role) {
        return result.filter(PDV => pdv.role === role);
      }
      return result;
    },
  },
};

export {
  PDVQueries as default,
};
