import { GraphQLList } from 'graphql';
import { createPDV } from '../../db/PDVsDb';
import CreatePDVInput from './createPDVInputType';
import PDV from './PDVType';

const PDVMutations = {
  createPDVs: {
    type: new GraphQLList(PDV),
    args: {
      input: {
        type: new GraphQLList(CreatePDVInput),
      },
    },
    resolve: async (source, { input }) => {
      const result = input.map(PDVPayload => createPDV(PDVPayload));
      return result;
    },
  },
};

export { PDVMutations as default };
