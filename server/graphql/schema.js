import {
  GraphQLSchema,
  GraphQLObjectType,
} from 'graphql';

import pdvQueries from './pdvs/PDVQueries';
import pdvMutations from './pdvs/PDVMutations';

export default new GraphQLSchema({
  query: new GraphQLObjectType({
    name: 'Query',
    fields: () => ({
      ...pdvQueries,
    }),
  }),
  mutation: new GraphQLObjectType({
    name: 'Mutation',
    fields: () => ({
      ...pdvMutations,
    }),
  }),
});
