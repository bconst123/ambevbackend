# Ambev Challenge
# Rodar o programa usando yarn

yarn run dev
  

http://localhost:3000/graphql
  
  
ps.: O JSON informado como entradas foi salvo em um array de objetos local e cada entrada também é adicionado nesse objeto que fica salvo enquanto o server.js estiver rodando. Optei por não enviar para um BD, visto que não era o intuito deste teste.
    
  
#Para testar rodar
  yarn test
  
  
# Para listar todos os PDVs que estão no BD Local + adicionados
  
query{
  	pdvs{
      	id
      	tradingName
      ownerName
      document
      coverageArea{ 
        type
        coordinates
      }
      address{ 
        type,
     	 coordinates
      }
    }
}
  
# Para pesquisar pelo id de um PDV
  
query{
  	pdv(id: "1"){
      	id
      	tradingName
      ownerName
      document
      coverageArea{ 
        type
        coordinates
      }
      address{ 
        type,
     	 coordinates
      }
    }
}

#Para procurar se um PVD está no Array MultiPolygon 
  
query{
  	searchpdv(x: -67.83039, y: -9.95782){,
        id
      	tradingName
      ownerName
      document
      coverageArea{ 
        type
        coordinates
      }
      address{ 
        type,
     	 coordinates
      }
    }
}


# para adicionar um novo PDV

mutation {
  createPDVs(input: [{tradingName: "Test", ownerName: "test", document: "t1est", coverageArea: [[[[-44.04912,-19.87743],[-44.0493,-19.89438],[-44.04758,-19.90212],[-44.04346,-19.90922],[-44.03385,-19.91923],[-44.01891,-19.92165],[-44.01647,-19.92306],[-44.01436,-19.92319],[-44.01175,-19.92427],[-44.00724,-19.92585],[-43.99909,-19.9185],[-43.99432,-19.91403],[-43.99557,-19.90842],[-43.99582,-19.90285],[-43.99436,-19.89002],[-43.99316,-19.8792],[-43.99436,-19.87371],[-43.99951,-19.86532],[-44.01917,-19.85135],[-44.02801,-19.8545],[-44.03745,-19.85668],[-44.04397,-19.8608],[-44.04912,-19.87743]]]], address: [-43.297337,-23.013538]}]) {
    	id
      tradingName
      ownerName
      document
      coverageArea{ 
        type
        coordinates
      }
      address{ 
        type,
     	 coordinates
      }
  }
}
